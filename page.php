<?php get_header(); ?>

<?php putRevSlider("homepage","homepage") ?>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main-page" class="large-12 medium-12 columns" role="main">

		    	<section class="homepage-top-text wow">
		    		
		    		<h2><span>Get your brand going</span> the smart, simple way</h2>
		    		<p>Tictail gives you more than an online store – it comes packed with the tools you need. ​From setting up smart Facebook campaigns to managing your social media channels and crafting beautiful email newsletters - wherever you want to take your brand, take it there with Tictail.</p>
		    		<a href="/contact/" class="rounded button">Start Your Free Consultation</a>

		    	</section>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>