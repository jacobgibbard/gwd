<section class="single-page-header">

<header class="article-header">	
	<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
	<h3><?php the_subtitle(); ?></h3>
	<p class="byline"><?php the_tags('<span class="tags-title">' . __( 'Work:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
</header> <!-- end article header -->

</section>

<section class="single-page-content">

<div id="inner-content" class="row">

	<main id="main" class="large-12 medium-12 small-12 columns" role="main">

		<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
							
		    <section class="entry-content" itemprop="articleBody">		
				<?php the_content(); ?>
			</section> <!-- end article section -->
											
			<?php comments_template(); ?>	
															
		</article> <!-- end article -->

	</main> <!-- end #main -->

</div> <!-- end #inner-content -->

</section>