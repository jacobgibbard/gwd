<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">					
	<div class="article-link">
		<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_subtitle(); ?></a></h2>
	</div> <!-- end article header -->									    						
</article> <!-- end article -->