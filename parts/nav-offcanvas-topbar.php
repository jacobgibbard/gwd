<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left float-left">
		<ul class="menu">
			<li class="name"><a href="<?php echo home_url(); ?>"><img src="/wp-content/uploads/logo.png" class="site-logo" /></a></li>
		</ul>
	</div>
	<div class="top-bar-right">
		<?php joints_top_nav(); ?>	
	</div>
</div>