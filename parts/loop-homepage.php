<div class="work-tile wow fadeIn">
	<a href="<?php the_permalink() ?>">
		<?php the_post_thumbnail('full'); ?>
		<div class="info wow fadeInTop">
		    <h4><?php the_title(); ?></h4>
		</div>
	</a>
</div>