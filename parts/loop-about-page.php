<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
					
    <h1><?php the_title(); ?></h1>
    <h2 class="serif"><?php the_subtitle(); ?></h2>

    <section class="entry-content" itemprop="articleBody">
	    <?php the_content(); ?>
	    <?php wp_link_pages(); ?>
	</section> <!-- end article section -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->