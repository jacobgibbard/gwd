					<br clear="both">
					<footer class="footer" role="contentinfo">
						<div class="center">
							<a href="/"><img src="/wp-content/uploads/gwave.png" /></a>
							<h4>Gibbard Web Design</h4>
						</div>
						<div id="inner-footer" class="row">							
							<div class="large-6 medium-6 columns text-left">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. <span class="small">Think for yourself. Question Authority.</span></p>
							</div>
							<div class="large-6 medium-6 columns text-right">
								<a href="/services/">Services</a> | <a href="/work/">Work</a> | <a href="/about/">About</a> | <a href="/category/blog/">Blog</a> | <a href="/contact/">Contact</a>
		    				</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->