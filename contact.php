<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<section id="contact-top">
	<h1>Hello. <span class="serif">Let's talk.</span></h1>
	<h2 class="serif">Ready to learn about you and your project needs. Call, email or fill out the form below.</h2>
</section>
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main-page" class="large-12 medium-12 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
