<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>

<?php putRevSlider( 'image-hero-services' ); ?>
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main-page" class="large-12 medium-12 columns" role="main">

		    	<section id="service-boxes">
		    		
		    		<!--<h2>Services</h2>-->
		    		<p class="bottompad">With over a decade of knowledge, here when you need it and securing the borders while you sleep. Proud to offer an unparalleled level of service and support that goes hand in hand. Providing solid solutions that help clients perform better - daily. Offering functionality that integrates simply with each business getting the attention and results needed.</p>
		    		
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-design.jpg" />
		    			<div class="service-info">
		    				<h3>Web Design</h3>
		    				<p>Custom responsive web design solutions to help your business thrive.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-responsive.jpg" />
		    			<div class="service-info">
		    				<h3>Responsively Built</h3>
		    				<p>Each site is made responsive with a mobile optimized layout.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-wordpress.jpg" />
		    			<div class="service-info">
		    				<h3>Wordpress</h3>
		    				<p>Specializing in custom themes and plugin development.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-ecommerce.jpg" />
		    			<div class="service-info">
		    				<h3>eCommerce</h3>
		    				<p>Solid solutions that perform with assistance when you really need it.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-hosting.jpg" />
		    			<div class="service-info">
		    				<h3>Hosting</h3>
		    				<p>Full hosting and maintenance packages to help you excel.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-search.jpg" />
		    			<div class="service-info">
		    				<h3>Search</h3>
		    				<p>The knowledge and know-how to get your business to the top.</p>
		    			</div>
		    		</div>	
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-development.jpg" />
		    			<div class="service-info">
		    				<h3>Development</h3>
		    				<p>Offer custom development assistance on a variety of platforms.</p>
		    			</div>
		    		</div>
		    		<div class="service-box">
		    			<img src="/wp-content/uploads/services-007.jpg" />
		    			<div class="service-info">
		    				<h3>Brand Assistance</h3>
		    				<p>Secretly helping brands and firms with some extra help when they need it.</p>
		    			</div>
		    		</div>  		

		    	</section>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
