<?php
/*
Template Name: Work
*/
?>

<?php get_header(); ?>

<?php putRevSlider( 'image-hero-work' ); ?>
			
	<div id="content">

		<section id="work-tiles">
			
			<h3 class="page-title">Recent Work</h3>

			<?php query_posts('cat=3'); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="homepage-tiles">
				<?php get_template_part( 'parts/loop', 'homepage' ); ?>
			</div>		    
			<?php endwhile; endif; ?>

		</section>

		<br clear="all" />	
	
		<div id="inner-content" class="row">
	
		    <main id="main-page" class="large-12 medium-12 columns" role="main">

				<section id="client-list">
			
				<h3 class="page-title wow fadeInBottom">Featured Clients</h3>

				<ul>
					<li>
						<img src="/wp-content/uploads/remington-1.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/r.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/hj.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/sds.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/ford.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/maxima-1.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/as.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/peacocks.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/fj.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/cci.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/sdg.jpg" class="wow fadeInLeft" />
					</li>
					<li>
						<img src="/wp-content/uploads/drd-1.jpg" class="wow fadeInLeft" />
					</li>
				</ul>

			</section>

			<br clear="all" />						

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
