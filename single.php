<?php get_header(); ?>
			
<div id="content">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		    <section id="work-tiles">

				<header class="article-header">
					<h3 class="page-title">More Work</h3>
				</header> <!-- end article header -->

				<?php query_posts('cat=3&posts_per_page=6'); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="homepage-tiles">
					<?php get_template_part( 'parts/loop', 'homepage' ); ?>
				</div>		    
				<?php endwhile; endif; ?>
				
			</section>	

</div> <!-- end #content -->

<?php get_footer(); ?>