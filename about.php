<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>

<?php putRevSlider( 'vimeo-hero' ); ?>
			
	<div id="content">

		    	<section class="about-top-text wow fadeInLeft">
		    		
		    		<h1>Over a decade strong.</h1>
		    		<h2>Creating hand-crafted experiences that help brands grow businesses and make lives better.</h2>

		    	</section>
				
				<section class="about-black">

					<div id="inner-content" class="row">
	
		    			<main id="main" class="large-12 medium-12 columns" role="main">
							
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php get_template_part( 'parts/loop', 'about-page' ); ?>
					
							<?php endwhile; endif; ?>

						</main> <!-- end #main -->
		    
					</div> <!-- end #inner-content -->

				</section>

				<br clear="all" />	

				<section class="about-quote wow fadeInLeft">
		    		
		    		<h3>"Today is your day! Your mountain is waiting. So... get on your way."</h3>
		    		<h4>- Theodor Geisel (Dr. Seuss)</h4>

		    	</section>	

		    	<br clear="all" />	

				<section id="client-list">

					<ul>
						<li>
							<img src="/wp-content/uploads/maxima-1.jpg" class="wow fadeInLeft" />
						</li>
						<li>
							<img src="/wp-content/uploads/as.jpg" class="wow fadeInLeft" />
						</li>
						<li>
							<img src="/wp-content/uploads/peacocks.jpg" class="wow fadeInLeft" />
						</li>
						<li>
							<img src="/wp-content/uploads/sdg.jpg" class="wow fadeInLeft" />
						</li>
					</ul>

				</section>						

	
	</div> <!-- end #content -->

<?php get_footer(); ?>
