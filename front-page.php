<?php
/*
Template Name: Homepage
*/
get_header(); ?>

<?php putRevSlider( 'web-product-dark' ); ?>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main-page" class="large-12 medium-12 columns" role="main">

		    	<section class="homepage-top-text wow fadeInLeft">
		    		
		    		<h1>San Diego Web Design and Development</h1>
		    		<h2>Focusing on <strong>Wordpress responsive</strong> development for high-impact results that help you perform better.</h2>

		    	</section>

		    	<section class="homepage-services wow fadeInBottom">
		    		
		    		<div class="row">
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-sitemap"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>Your brand. Your way.</h3>
			    				<p>Your vision. Your design. Your domain. All the way you want it.</p>
			    			</div>
			    		</div>
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-mobile"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>Designs, that respond.</h3>
			    				<p>Mobile first designs. For your needs. The way you need it.</p>
			    			</div>
			    		</div>
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-line-chart"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>eCommerce. Big and Small.</h3>
			    				<p>Small Storefront. Big Powerhouse. Solutions you can trust.</p>
			    			</div>
			    		</div>
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-rocket"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>Search. And destroy.</h3>
			    				<p>Be heard. Listen wisely. Shout proudly.</p>
			    			</div>
			    		</div>
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-wordpress"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>Wordpress? We can help.</h3>
			    				<p>Basic maintenance. Custom development. Let's do it.</p>
			    			</div>
			    		</div>
			    		<div class="large-4 medium-6 small-12 columns">
			    			<div class="small-3 columns home-icon">
			    				<i class="fa fa-inbox"></i>
			    			</div>
			    			<div class="small-9 columns">			    				
			    				<h3>Hello. Let's Talk.</h3>
			    				<p>Take pride in ownership. Promises backed with trust.</p>
			    			</div>
			    		</div>
		    		</div>

		    	</section>						
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

		<section id="work-tiles">
			
			<h3>Recent Work</h3>

			<?php query_posts('cat=3'); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="homepage-tiles">
				<?php get_template_part( 'parts/loop', 'homepage' ); ?>
			</div>		    
			<?php endwhile; endif; ?>

		</section>	

		<br clear="all" />

		<section id="contact-form" class="wow fadeInRight">

			<h2>Hi. Let's talk.</h2>
			<h3>Have an idea or need a quote?</h3>
			<?php echo do_shortcode( '[contact-form-7 id="84" title="Contact Form"]' ); ?>

		</section>	

	</div> <!-- end #content -->

	

<?php get_footer(); ?>